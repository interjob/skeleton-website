Form = {};


Form.logOut = function(){
   // $.cookie("accessKey", null,{ expires : 1 });   
    $.cookie('accessKey', null, { path: '/' });
    //$.removeCookie("accessKey");
    $.removeCookie('accessKey', { path: '/' });    
       
   
    
    accessKey="";
    setTimeout("Form.redirect('/')",1200); 
  //  Form.redirect("/");
}

Form.ajaxCall = function(dataRequest){

	$.ajax({
			url: dataRequest.url,
			type: dataRequest.methodForm,
			data: dataRequest.formData,
                        dataType: dataRequest.dataType,

			//crossDomain: dataRequest.crossDomain,
			beforeSend: function(jqXHR, settings){
	        	if(dataRequest.beforeSendHandler != false)
	        		eval(dataRequest.beforeSendHandler)(jqXHR, settings);
                        },
			success: function(data, textStatus, jqXHR){
				if(dataRequest.successHandler != false)
	        		eval(dataRequest.successHandler);
			},
			error: function(jqXHR, textStatus, errorThrown){
	        	if(dataRequest.errorHandler != false)
	        		eval(dataRequest.errorHandler)(jqXHR, textStatus, errorThrown);
                        },
                        complete: function(data, textStatus, jqXHR){
				if(dataRequest.completeHandler != false)
	        		eval(dataRequest.completeHandler);
			},
			cache: dataRequest.cache,
                        contentType: dataRequest.contentType,
                        processData: dataRequest.processData
		});
};


Form.attachEventOne = function(element, event, callback){
	$(element).one(event, function(){
		eval(callback);
		return false;
	});
};

Form.redirect = function(redirecturl){
    window.location.replace(redirecturl);
}

Form.isValidEmailAddress = function(emailAddress){
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(emailAddress);
};

Form.validateImg = function(file) {
	if(file === undefined)
		return false;

	var supportedFormat = ['image/jpeg', 'image/png', 'image/gif'];
	if($.inArray(file.type, supportedFormat)<0){
		//console.log('Wrong type');
		return false;
	}
	if(file.size > 3000000){
		//console.log('Too big!!!');
		return false;
	}
	return true;
};