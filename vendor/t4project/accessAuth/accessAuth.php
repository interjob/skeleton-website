<?php
//*Namespace T4
namespace T4;
//* add PDO in T4 namespace to allow use
use PDO;
//*AccessAuth class
/**
	This class is a simple authetication for async necessities, this minimize to a single string the authetion parameter need to autheticate
	every autorization needed action.
*/
class AccessAuth{

	//*PDO Resource
	/**
		$db data member need to be injected in class initialization
	*/
	protected $db;

	//*interger
	/**
		$userID data member is the table id pk to identify users
	*/
	public $userID = null;
        
        //*interger
	/**
		$authLevel data member is the table users' authorization level
	*/
	public $authLevel = null;

	//*interger
	/**
		$accessKey data member is the table userToken pk to identify users and get authorization if they have
	*/
	public $accessKey = null;

	//*construct method
	/**
		AccessAuth start with DEBUG mode off, this class need a db interface passed parameter to use every method
		$db : PDO Resource
		$debug : boolean
	*/

	function __construct($db, $debug = false){
		$this->db = $db;
		if($debug === true)
		{
			$this->TestThis();
		}
	}

	//*private method to check user table and eventually call the reset method
	/**
		TestThis is a private method call only in DEBUG mode that attempt to get user if fail reset authetication system to factory setting
	*/

	private function TestThis(){
		try
		{
			$stmt = $this->db->prepare("SELECT id FROM authuser");
			$stmt->execute();
			$rs = $stmt->fetchAll(PDO::FETCH_ASSOC);
			if(count($rs)!=1)
				$this->reset();
		}
		catch(Exception $e)
		{
			$this->reset();
		}
	}

	//*private method to init or reinit user table
	/**
		reset initialize or reinitialize the user table from sql prepared, it is call only in DEBUG mode
	*/

	private function reset(){
		$sql = file_get_contents(BASE_PATH."/../vendor/t4project/accessAuth/accessAuth.sql");
		$stmt = $this->db->prepare($sql);
		$stmt->execute();
	}

	//*private  method to log user signin
	/**
		update user lastlogin column
	*/
	private function logIt()
	{
		$stmt = $this->db->prepare("UPDATE authuser SET lastlogin = now() WHERE id = :id");
		$stmt->execute(array(':id' => $this->userID ));
		$affected_rows = $stmt->rowCount();

		if($affected_rows <= 0)
			print_r($this->db->errorInfo());
	}

	//*private method for login through user/password authetication
	/**
		login get username and password to check it in autorized user table and get data about it
		$username : string
		$password : string
	*/

	private function login($username, $password){

		$stmt = $this->db->prepare("SELECT authuser.id, authuser.authlvl, authtoken.accesskey FROM authuser INNER JOIN authtoken ON authuser.id = authtoken.authuserid WHERE authuser.email = :username AND authuser.pass = MD5( :password )");
		$stmt->execute(array(':username' => $username, ':password' => $password));

		$rs = $stmt->fetchAll(PDO::FETCH_ASSOC);

		if( !empty( $rs ) )
		{
			$this->userID = $rs[0]['id'];
                        $this->authLevel = $rs[0]['authlvl'];
			$this->accessKey = $rs[0]['accesskey'];
			$this->logIt();
		}

		return ( !empty( $rs ) );
	}

	//*private method to check keys
	/**
		hasKey check every passed key for autorize actions, return true or false
		$key : string
	*/

	private function hasKey($key)
	{
		$stmt = $this->db->prepare("SELECT authuser.id ,authuser.authlvl FROM authtoken INNER JOIN authuser ON authuser.id = authtoken.authuserid WHERE accesskey = ?");
		$stmt->execute(array($key));
		$rs = $stmt->fetchAll(PDO::FETCH_ASSOC);
		if(!empty( $rs )){
			$this->accessKey = $key;
			$this->userID = $rs[0]['id'];
                        $this->authLevel = $rs[0]['authlvl'];
			return true;
		}
		return false;
	}

	//*private method to regenerate AccessKey
	/**
		getNewKey generate new Key and set it in db for next action's authentication, return the new key or false if something went wrong
	*/

	private function getNewKey()
	{
		$newKey = $this->userID.'_'.uniqid();
		$stmt = $this->db->prepare("UPDATE authToken SET accesskey = :newAccessKey WHERE accesskey = :accessKey");
		$stmt->execute(array( ':newAccessKey' => $newKey, ':accessKey' => $this->accessKey ));
		$affected_rows = $stmt->rowCount();
		return ($affected_rows > 0 ) ? $newKey : false;
	}

	//* Unique public method to start Authetication algorithm
	/**
		autheticateMe check the accessKey or attempt to gain it by username and password, if fail return null
		<pre>
		$data : array(
			username 	string (opt)
			password 	string (opt)
			key 		string (opt)
		)
		</pre>
	*/

	public function autheticateMe($data){

		$key = (isset($data['key'])) ? $data['key'] : null;
		if(!$this->hasKey($key)){
			if(!isset($data['username']) || !isset($data['password']) )
				return null;
			else{
				$username = (isset($data['username'])) ? $data['username'] : null;
				$password = (isset($data['password'])) ? $data['password'] : null;

				$logIn = $this->login($username, $password);

				if($logIn)
					$key = $this->getNewKey();
				return $key;
			}
		}else{
			$key = $this->getNewKey();
			return $key;
		}
	}

}