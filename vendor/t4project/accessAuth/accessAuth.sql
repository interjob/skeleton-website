-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Giu 06, 2015 alle 11:59
-- Versione del server: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `linkforall`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `authtoken`
--

CREATE TABLE IF NOT EXISTS `authtoken` (
  `authUserID` int(11) NOT NULL,
  `accessKey` varchar(255) NOT NULL,
  `authLvl` int(11) NOT NULL,
  `createdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedat` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `authtoken`
--

INSERT INTO `authtoken` (`authUserID`, `accessKey`, `authLvl`, `createdat`, `updatedat`) VALUES
(1, '1_asdsddvsdggc', 0, '2013-11-05 13:45:37', '2013-11-05 13:45:37');

-- --------------------------------------------------------

--
-- Struttura della tabella `authuser`
--

CREATE TABLE IF NOT EXISTS `authuser` (
`id` int(11) NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `pass` varchar(255) CHARACTER SET utf8 NOT NULL,
  `lastlogin` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `createdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedat` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dump dei dati per la tabella `authuser`
--

INSERT INTO `authuser` (`id`, `email`, `pass`, `lastlogin`, `createdat`, `updatedat`) VALUES
(1, 'admin', '797cb93f8b1159e6dc68b2b7fddd6c55', '2015-06-06 08:25:27', '2013-11-05 13:45:37', '2013-11-05 13:45:37');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `authtoken`
--
ALTER TABLE `authtoken`
 ADD PRIMARY KEY (`authUserID`);

--
-- Indexes for table `authuser`
--
ALTER TABLE `authuser`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `authuser`
--
ALTER TABLE `authuser`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;