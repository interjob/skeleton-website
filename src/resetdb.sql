-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 07, 2015 alle 15:22
-- Versione del server: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `linkforall`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `accounts`
--

DROP TABLE IF EXISTS `accounts`;
CREATE TABLE IF NOT EXISTS `accounts` (
`id` int(11) NOT NULL,
  `authuser_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `birthplace` varchar(255) NOT NULL,
  `birthdate` date NOT NULL,
  `sex` enum('M','F') NOT NULL,
  `profile_img` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `address_civic` varchar(255) NOT NULL,
  `zipcode` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `states` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `vatcode` varchar(255) NOT NULL,
  `privacy` tinyint(1) NOT NULL DEFAULT '0',
  `newsletter` tinyint(1) NOT NULL DEFAULT '0',
  `createdat` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatedat` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dump dei dati per la tabella `accounts`
--

INSERT INTO `accounts` (`id`, `authuser_id`, `name`, `surname`, `birthplace`, `birthdate`, `sex`, `profile_img`, `phone`, `address`, `address_civic`, `zipcode`, `city`, `states`, `country`, `vatcode`, `privacy`, `newsletter`, `createdat`, `updatedat`, `active`) VALUES
(1, 1, 'nometest', 'cognometest', 'birthplace', '2015-06-09', 'M', 'profile.jpg', '0392321323', 'via pion ', '2', '23213', 'monza', 'mb', 'italia', 'vatcode', 1, 1, '2015-06-09 18:14:16', '0000-00-00 00:00:00', 1),
(2, 0, 'test2', 'test2', 'test2', '0000-00-00', 'M', 'test2', '0362235185', 'via lombardia 4', '232', '20222', 'Seregno', 'MB', 'Italia', 'test2', 1, 1, '2015-06-11 19:48:06', '0000-00-00 00:00:00', 1),
(3, 0, 'test3', 'test2', 'test2', '0000-00-00', 'M', 'test2', '0362235185', 'via lombardia 4', '232', '20222', 'Seregno', 'MB', 'Italia', 'test2', 1, 1, '2015-06-11 19:51:15', '0000-00-00 00:00:00', 1),
(4, 0, 'test3', 'test2', 'test2', '2015-06-09', 'M', 'test2', '0362235185', 'via lombardia 4', '232', '20222', 'Seregno', 'MB', 'Italia', 'test2', 1, 1, '2015-06-11 19:51:43', '0000-00-00 00:00:00', 1),
(5, 0, 'test3', 'test2', 'test2', '2015-06-09', 'M', 'test2', '0362235185', 'via lombardia 4', '232', '20222', 'Seregno', 'MB', 'Italia', 'test2', 1, 1, '2015-06-11 20:04:48', '0000-00-00 00:00:00', 1),
(6, 0, 'test2', 'test2', 'dsadas', '2015-06-09', '', '', '0362235185', 'via lombardia 4', '', '20222', 'Seregno', 'MB', 'Italia', 'dsdasdas', 2, 1, '2015-06-11 20:05:32', '0000-00-00 00:00:00', 1),
(7, 0, 'test2', 'test2', 'test2', '2015-06-09', 'M', 'test2', '0362235185', 'info@interjob.it', '232', '20222', 'Seregno', 'MB', 'Italia', 'test2', 1, 0, '2015-06-11 20:10:56', '0000-00-00 00:00:00', 1),
(8, 0, 'test2', 'test2', 'test2', '2015-06-09', 'M', 'test2', '0362235185', 'info@interjob.it', '', '20222', 'Seregno', 'MB', 'Italia', 'test2', 1, 1, '2015-06-11 20:12:01', '0000-00-00 00:00:00', 1),
(9, 0, 'test2', 'test2', 'test2', '2015-06-09', 'M', 'test2', '0362235185', 'info@interjob.it', '232', '20222', 'Seregno', 'MB', 'Italia', 'test2', 2, 0, '2015-06-11 20:15:25', '0000-00-00 00:00:00', 1),
(10, 0, 'test2', 'test2', 'test2', '2015-06-09', 'M', 'test2', '0362235185', 'info@interjob.it', '232', '20222', 'Seregno', 'MB', 'Italia', 'test2', 2, 0, '2015-06-11 20:16:18', '0000-00-00 00:00:00', 1),
(11, 2, 'test2xxde', 'test2', 'test2', '2015-06-09', 'M', 'test2', '0362235185', 'via lombardia 4', '232', '20222', 'Seregno', 'MB', 'Italia', 'test2', 1, 1, '2015-06-11 20:18:54', '0000-00-00 00:00:00', 1),
(12, 3, 'test2', 'test2', 'test2', '2015-06-09', 'M', 'Europass-CV-20130911-Peri-IT.pdf', '0362235185', 'via lombardia 4', '232', '20222', 'Seregno', 'MB', 'Italia', 'test2', 1, 0, '2015-06-11 20:38:59', '0000-00-00 00:00:00', 1),
(13, 9, 'x', 'x', 'x', '2015-07-31', 'M', '', '0362235185', 'via lombardia 4', '6', '20222', 'Seregno', 'MB', 'Italia', 'bnfdnl82p29f704I', 0, 0, '2015-07-18 14:04:33', '0000-00-00 00:00:00', 1),
(14, 10, 'Gino', 'Pilotos', 'Salcazzo', '2015-09-01', 'M', '', '123123123', 'via le mani dal culo', '5', '20057', 'SalcazzoCity', 'Paese dei balocchi', 'Italy', 'MLOLSS87M20F704D', 0, 0, '2015-09-19 10:00:05', '0000-00-00 00:00:00', 1),
(15, 11, 'Pippo', 'Pippo', 'Pippa', '2015-09-05', 'F', '', '123123123', 'piazza la bomba e scappa', '1', '12345', 'suca', 'cerizzi culo', 'italy', 'bnfdnl82p29f704I', 1, 1, '2015-09-19 10:14:00', '0000-00-00 00:00:00', 1),
(16, 12, 'Test', 'Test', 'Salcazzo', '2015-09-02', 'M', '', '123123123', 'via minchia', '9', '12345', 'sucacity', 'stato cazzo', 'cerizzi culo', 'bnfdnl82p29f704I', 1, 1, '2015-09-19 10:21:07', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `authtoken`
--

DROP TABLE IF EXISTS `authtoken`;
CREATE TABLE IF NOT EXISTS `authtoken` (
  `authUserID` int(11) NOT NULL,
  `accessKey` varchar(255) NOT NULL,
  `createdat` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatedat` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `authtoken`
--

INSERT INTO `authtoken` (`authUserID`, `accessKey`, `createdat`, `updatedat`) VALUES
(1, '2848dc7d3b44bd213657fa9e85398bf9cca7394f603a1f039714c96e0cf309fc', '2013-11-05 13:45:37', '2015-11-07 12:38:31'),
(12, '825f44f68b827c1c49047e7a54f754142dd1a9830cd922a6073666f7129b06bf', '2015-09-19 10:08:27', '2015-09-19 10:21:44');

-- --------------------------------------------------------

--
-- Struttura della tabella `authuser`
--

DROP TABLE IF EXISTS `authuser`;
CREATE TABLE IF NOT EXISTS `authuser` (
`id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `authLvl` int(11) NOT NULL,
  `lastlogin` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `createdat` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatedat` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dump dei dati per la tabella `authuser`
--

INSERT INTO `authuser` (`id`, `email`, `pass`, `authLvl`, `lastlogin`, `createdat`, `updatedat`) VALUES
(1, 'admin@admin.com', 'a5d69dffbc219d0c0dd0be4a05505b219b973a04b4f2cd1979a5c9fd65bc03622e4417ec767ffe269e5e53f769ffc6e16bf05796fddf2771e4dc6d1cb2ac3fcf', 2, '2015-11-07 12:38:31', '2013-11-05 13:45:37', '2013-11-05 13:45:37'),
(2, 'info@interjob.it', 'a5d69dffbc219d0c0dd0be4a05505b219b973a04b4f2cd1979a5c9fd65bc03622e4417ec767ffe269e5e53f769ffc6e16bf05796fddf2771e4dc6d1cb2ac3fcf', 2, '2015-06-27 08:46:25', '2015-06-11 20:18:54', '0000-00-00 00:00:00'),
(3, 'info@pota.it', 'a5d69dffbc219d0c0dd0be4a05505b219b973a04b4f2cd1979a5c9fd65bc03622e4417ec767ffe269e5e53f769ffc6e16bf05796fddf2771e4dc6d1cb2ac3fcf', 1, '2015-06-27 08:46:31', '2015-06-11 20:38:59', '0000-00-00 00:00:00'),
(8, 'test@mailinator.com', 'a5d69dffbc219d0c0dd0be4a05505b219b973a04b4f2cd1979a5c9fd65bc03622e4417ec767ffe269e5e53f769ffc6e16bf05796fddf2771e4dc6d1cb2ac3fcf', 1, '2015-06-27 08:46:40', '2015-06-11 21:20:20', '0000-00-00 00:00:00'),
(9, 'info@interjob.it', 'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e', 1, '2015-07-18 14:04:33', '2015-07-18 14:04:33', '0000-00-00 00:00:00'),
(10, 'test10@mailinator.com', 'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e', 1, '2015-09-19 10:00:05', '2015-09-19 10:00:05', '0000-00-00 00:00:00'),
(11, 'pippo@pippo.it', 'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e', 1, '2015-09-19 10:14:00', '2015-09-19 10:14:00', '0000-00-00 00:00:00'),
(12, 'salcazzo@mailinator.com', 'a5d69dffbc219d0c0dd0be4a05505b219b973a04b4f2cd1979a5c9fd65bc03622e4417ec767ffe269e5e53f769ffc6e16bf05796fddf2771e4dc6d1cb2ac3fcf', 1, '2015-09-19 10:21:44', '2015-09-19 10:21:07', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struttura della tabella `companies`
--

DROP TABLE IF EXISTS `companies`;
CREATE TABLE IF NOT EXISTS `companies` (
`id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `profile_id` int(11) NOT NULL,
  `company_slug` varchar(255) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `company_subtitle` varchar(255) NOT NULL,
  `company_logo` varchar(255) NOT NULL,
  `company_city` varchar(255) NOT NULL,
  `company_state` varchar(255) NOT NULL,
  `company_country` varchar(255) NOT NULL,
  `company_address` varchar(255) NOT NULL,
  `company_url` varchar(255) NOT NULL,
  `company_baseinfo` text NOT NULL,
  `company_description` text NOT NULL,
  `company_services` text NOT NULL,
  `social_facebook_link` varchar(255) NOT NULL,
  `social_linkedin_link` varchar(255) NOT NULL,
  `company_headerimg` varchar(255) NOT NULL,
  `company_keywords` text NOT NULL,
  `createdat` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatedat` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

--
-- Dump dei dati per la tabella `companies`
--

INSERT INTO `companies` (`id`, `account_id`, `profile_id`, `company_slug`, `company_name`, `company_subtitle`, `company_logo`, `company_city`, `company_state`, `company_country`, `company_address`, `company_url`, `company_baseinfo`, `company_description`, `company_services`, `social_facebook_link`, `social_linkedin_link`, `company_headerimg`, `company_keywords`, `createdat`, `updatedat`, `active`) VALUES
(1, 1, 3, 'test', 'test azienda', '', '', 'Seregno', 'MB', 'Italia', 'via lombardia 4', 'www.sd', '<p>test</p>\r\n', '', '', 'faceb', 'linke', 'testat', '23413;sda', '2015-06-18 17:28:02', '2015-09-19 09:44:41', 0),
(2, 1, 1, 'trest2', 'test2', '', '', '', '', '', 'sxs', '1', '1', '', '', '1', '11', '1', '1', '2015-06-18 17:53:53', '2015-09-12 10:20:58', 0),
(3, 3, 1, 'testaz', 'test azienda', '', '', 'Seregno', 'Monza e della Brianza', 'Italia', 'Via amburgo 20', 'www.sd', '<p>xxx</p>\r\n', '', '', 'faceb', 'linke', 'testat', '23413;sda', '2015-06-23 21:08:29', '2015-09-12 10:20:58', 1),
(4, 1, 1, 'companyname_8', 'companyname_8', 'dsa', '', '', '', '', '', 'dsa', 'da', 'sda', 'dsadsa', 'das', 'dsa', '', 'companyname_8', '2015-07-25 13:46:23', '2015-10-24 12:24:34', 1),
(5, 1, 1, 'companyname_5', 'companyname_5', '', '', 'companyname_5', 'companyname_5', 'companyname_5', 'companyname_5', 'companyname_5', '', 'companyname_5', '', 'companyname_5', 'companyname_5', '', 'companyname_5', '2015-07-25 13:46:57', '2015-09-12 10:32:08', 1),
(6, 1, 1, 's', 's', '', '', 's', 's', 's', 's', 'a', 's', ' s', '', 'd', 'a', '', 'a', '2015-07-25 14:07:29', '2015-10-24 10:14:53', 0),
(7, 1, 1, 'a', 'a', '', '', 'd', 'e', 'f', 'c', 'g', ' ', ' g', '', 'd', 's', '', 's', '2015-07-25 14:08:44', '2015-09-12 10:31:47', 1),
(10, 1, 1, 'as', 'as', '', '', 'Seregno', 'MB', 'Italia', 'a', 'sda', ' ', ' ', '', 'd', 's', '', 'a', '2015-07-25 14:12:19', '2015-09-12 09:58:05', 1),
(11, 1, 1, 'asdsad', 'asdsad', '', '', 'guiyg', 'asd', 'sda', 'asd', 'asd', ' ', 'sad ', '', 'asd', 'sdasd', '', 'asd', '2015-07-25 14:13:59', '2015-09-12 10:33:01', 0),
(12, 1, 1, 'xx', 'xx', '', '', 'sd', 'd', 'ddsds', 'sdq', 'asd', ' ', 'sd ', '', 'ds', 'sd', '', 'dsad', '2015-07-25 14:16:26', '2015-09-12 09:50:24', 1),
(13, 1, 1, 'final', 'final', '', '', 'final', 'final', 'final', 'final', 'final', 'final ', 'final ', '', 'final', 'final', '', 'final', '2015-07-25 14:20:35', '2015-09-12 10:32:58', 0),
(14, 1, 1, 'testo10', 'testo10', '', '', '', '', '', '', 'x', ' x', ' x', '', 'x', 'x', '', 'x', '2015-10-03 11:22:09', '0000-00-00 00:00:00', 1),
(15, 1, 1, 'test11', 'test11', '', '', '', '', '', '', 'test11', ' test11', ' test11', '', 'test11', 'test11', '', 'test11', '2015-10-03 11:24:53', '0000-00-00 00:00:00', 1),
(16, 1, 1, 'test12', 'test12', '', '', '', '', '', '', 'test12', ' test12', ' test12', '', 'test12', 'test12', '', 'test12', '2015-10-03 11:26:50', '0000-00-00 00:00:00', 1),
(17, 1, 1, 'test13', 'test13', '', '', '', '', '', '', 'test13', ' test13', ' test13', '', 'test13', 'test13', '', 'test13', '2015-10-03 11:27:59', '0000-00-00 00:00:00', 1),
(18, 1, 1, 'test14', 'test14', '', '', '', '', '', '', 'test14', ' test14', ' test14', '', 'test14', 'test14', '', 'test14', '2015-10-03 11:30:04', '0000-00-00 00:00:00', 1),
(19, 1, 1, 'test15', 'test15', '', '', '', '', '', '', 'test15', ' test15', ' test15', '', 'test15', 'test15', '', 'test15', '2015-10-03 11:32:02', '0000-00-00 00:00:00', 1),
(20, 1, 1, 'test16', 'test16', '', '', '', '', '', '', 'test16', ' test16', ' test16', '', 'test16', 'test16', '', 'test16', '2015-10-03 11:34:58', '0000-00-00 00:00:00', 1),
(21, 1, 1, 'test17', 'test17', '', '', '', '', '', '', 'test17', ' test17', ' test17', '', 'test17', 'test17', '', 'test17', '2015-10-03 11:36:38', '0000-00-00 00:00:00', 1),
(22, 1, 1, 'test18', 'test18', '', '', '', '', '', '', 'test18', ' test18', ' test18', '', 'test18', 'test18', '', 'test18', '2015-10-03 11:39:11', '0000-00-00 00:00:00', 1),
(23, 1, 1, 'test19', 'test19', '', '', '', '', '', '', 'test19', ' test19', ' test19', '', 'test19', 'test19', '', 'test19', '2015-10-03 11:43:02', '0000-00-00 00:00:00', 1),
(24, 1, 1, 'test20', 'test20', '', '', '', '', '', '', 'test20', ' test20', ' test20', '', 'test20', 'test20', '', 'test20', '2015-10-03 11:45:16', '0000-00-00 00:00:00', 1),
(25, 1, 1, 'test21', 'test21', '', '', '', '', '', '', 'test21', ' test21', ' test21', '', 'test21', 'test21', '', 'test21', '2015-10-03 11:46:08', '0000-00-00 00:00:00', 1),
(26, 1, 1, 'test22', 'test22', '', '', '', '', '', '', 'test22', ' test22', ' test22', '', 'test22', 'test22', '', 'test22', '2015-10-03 11:47:12', '0000-00-00 00:00:00', 1),
(27, 1, 1, 'test24', 'test24', '', '', '', '', '', '', 'test24', ' test24', ' test24', '', 'test24', 'test24', '', 'test24', '2015-10-03 11:49:48', '0000-00-00 00:00:00', 1),
(28, 1, 1, 'test25', 'test25', '', '', '', '', '', '', 'test25', ' test25', ' test25', '', 'test25', 'test25', '', 'test25', '2015-10-03 11:54:30', '0000-00-00 00:00:00', 1),
(29, 1, 1, '560fb5e11f3b2-test26-', 'test26', '', '', '', '', '', '', 'test26', ' test26', ' test26', '', 'test26test26', 'test26', '', 'test26', '2015-10-03 13:02:57', '0000-00-00 00:00:00', 1),
(30, 1, 1, '560fb769bb5f2-test28-', 'test28', '', '', '', '', '', '', 'test28', ' test28', ' test28', '', 'test28', 'test28', '', 'test28', '2015-10-03 13:09:29', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `companies_addresses`
--

DROP TABLE IF EXISTS `companies_addresses`;
CREATE TABLE IF NOT EXISTS `companies_addresses` (
`id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `cap` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `latgeo` varchar(255) NOT NULL,
  `longgeo` varchar(255) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `createdat` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatedat` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dump dei dati per la tabella `companies_addresses`
--

INSERT INTO `companies_addresses` (`id`, `company_id`, `address`, `city`, `state`, `cap`, `country`, `description`, `latgeo`, `longgeo`, `active`, `createdat`, `updatedat`) VALUES
(1, 28, 'Via Lombardia, 41', 'Santa Margherita', 'MB', '20851', 'Italia', '', '45.633003', '9.226969000000054', 0, '2015-10-03 11:54:30', '2015-11-07 12:05:16'),
(2, 29, 'Via Giovanni Berchet, 1', 'Monza', 'MB', '20900', 'Italia', '', '45.5813043', '9.264026300000069', 1, '2015-10-03 13:02:57', '0000-00-00 00:00:00'),
(3, 30, 'Via Bellinzona, 1', 'Como', 'CO', '22100', 'Italia', '', '45.8188766', '9.062865399999964', 1, '2015-10-03 13:09:29', '0000-00-00 00:00:00'),
(4, 28, 'SDA Street, ', 'Kumasi', 'Kumasi Metropolitan', '', 'Ghana', '', 'undefined', 'undefined', 1, '2015-11-07 12:47:02', '0000-00-00 00:00:00'),
(5, 28, 'Via Savona, 11', 'Sesto San Giovanni', 'MI', '20099', 'Italia', '', 'undefined', 'undefined', 1, '2015-11-07 12:54:48', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struttura della tabella `companies_gallery`
--

DROP TABLE IF EXISTS `companies_gallery`;
CREATE TABLE IF NOT EXISTS `companies_gallery` (
`id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `createdat` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatedat` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dump dei dati per la tabella `companies_gallery`
--

INSERT INTO `companies_gallery` (`id`, `company_id`, `title`, `image`, `createdat`, `updatedat`, `active`) VALUES
(3, 1, 'rr', '', '2015-06-29 19:35:04', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `companies_typologies`
--

DROP TABLE IF EXISTS `companies_typologies`;
CREATE TABLE IF NOT EXISTS `companies_typologies` (
`id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `createdat` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatedat` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dump dei dati per la tabella `companies_typologies`
--

INSERT INTO `companies_typologies` (`id`, `title`, `createdat`, `updatedat`, `active`) VALUES
(1, 'companies_typologies_title_Abbigliamento-Accessori', '2015-06-23 17:44:18', '0000-00-00 00:00:00', 1),
(2, 'companies_typologies_title_Abbigliamento-Biancheria, asciugamani, lenzuola', '2015-06-23 17:46:17', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `companies_typologies_old`
--

DROP TABLE IF EXISTS `companies_typologies_old`;
CREATE TABLE IF NOT EXISTS `companies_typologies_old` (
`id` int(11) NOT NULL,
  `parent_category` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `company_typologies_link`
--

DROP TABLE IF EXISTS `company_typologies_link`;
CREATE TABLE IF NOT EXISTS `company_typologies_link` (
`id` int(11) NOT NULL,
  `id_company` int(11) NOT NULL,
  `id_typology` int(11) NOT NULL,
  `createdat` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatedat` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Dump dei dati per la tabella `company_typologies_link`
--

INSERT INTO `company_typologies_link` (`id`, `id_company`, `id_typology`, `createdat`, `updatedat`) VALUES
(18, 1, 1, '2015-06-23 21:07:57', '0000-00-00 00:00:00'),
(19, 3, 1, '2015-06-23 21:08:29', '0000-00-00 00:00:00'),
(20, 30, 1, '2015-10-03 13:09:29', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struttura della tabella `expires_status`
--

DROP TABLE IF EXISTS `expires_status`;
CREATE TABLE IF NOT EXISTS `expires_status` (
`id` int(11) NOT NULL,
  `code_company` varchar(255) NOT NULL,
  `code_profile` varchar(255) NOT NULL,
  `date_init` date NOT NULL,
  `date_end` date NOT NULL,
  `createdat` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatedat` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `languages`
--

DROP TABLE IF EXISTS `languages`;
CREATE TABLE IF NOT EXISTS `languages` (
`id` int(11) NOT NULL,
  `language_code` varchar(255) NOT NULL,
  `flag` varchar(255) NOT NULL,
  `language` varchar(255) NOT NULL,
  `createdat` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatedat` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `languages_i18n`
--

DROP TABLE IF EXISTS `languages_i18n`;
CREATE TABLE IF NOT EXISTS `languages_i18n` (
`id` int(11) NOT NULL,
  `code_key` varchar(255) NOT NULL,
  `language_code` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `createdat` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatedat` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dump dei dati per la tabella `languages_i18n`
--

INSERT INTO `languages_i18n` (`id`, `code_key`, `language_code`, `value`, `createdat`, `updatedat`) VALUES
(1, 'profiles_types_title_free', 'IT', 'Free', '2015-06-09 17:34:02', '0000-00-00 00:00:00'),
(2, 'companies_typologies_title_Abbigliamento-Accessori', 'IT', 'Abbigliamento-Accessori', '2015-06-23 17:42:14', '0000-00-00 00:00:00'),
(3, 'companies_typologies_title_Abbigliamento-Biancheria, asciugamani, lenzuola', 'IT', 'Biancheria, asciugamani, lenzuola', '2015-06-23 17:45:37', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struttura della tabella `log`
--

DROP TABLE IF EXISTS `log`;
CREATE TABLE IF NOT EXISTS `log` (
`id` int(11) NOT NULL,
  `authuserid` int(11) NOT NULL,
  `action` text NOT NULL,
  `logdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `payments`
--

DROP TABLE IF EXISTS `payments`;
CREATE TABLE IF NOT EXISTS `payments` (
`id` int(11) NOT NULL,
  `code_profiles` int(11) NOT NULL,
  `code_company` int(11) NOT NULL,
  `id_paypal` varchar(255) NOT NULL,
  `id_order` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `createdat` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatedat` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `profiles_features`
--

DROP TABLE IF EXISTS `profiles_features`;
CREATE TABLE IF NOT EXISTS `profiles_features` (
`id` int(11) NOT NULL,
  `profile_id` int(11) NOT NULL,
  `badge_sitelink` tinyint(1) NOT NULL DEFAULT '0',
  `badge_companysheet` tinyint(1) NOT NULL DEFAULT '0',
  `search_category` tinyint(1) NOT NULL DEFAULT '0',
  `search_location` tinyint(1) NOT NULL DEFAULT '0',
  `search_keywords` tinyint(1) NOT NULL DEFAULT '0',
  `company_baseinfo` tinyint(1) NOT NULL DEFAULT '0',
  `company_description` tinyint(1) NOT NULL DEFAULT '0',
  `company_address` tinyint(1) NOT NULL DEFAULT '0',
  `company_multipleaddress` tinyint(1) NOT NULL DEFAULT '0',
  `company_sociallink` tinyint(1) NOT NULL DEFAULT '0',
  `company_services` tinyint(1) NOT NULL DEFAULT '0',
  `company_reviews` tinyint(1) NOT NULL DEFAULT '0',
  `company_favorites` tinyint(1) NOT NULL DEFAULT '0',
  `company_headerimg` tinyint(1) NOT NULL DEFAULT '0',
  `company_promo` tinyint(1) NOT NULL DEFAULT '0',
  `company_hr` tinyint(1) NOT NULL DEFAULT '0',
  `company_photogallery` tinyint(1) NOT NULL DEFAULT '0',
  `company_keywords` int(11) NOT NULL DEFAULT '0',
  `company_categories` int(11) NOT NULL DEFAULT '0',
  `extra_homecompanybanner` tinyint(1) NOT NULL DEFAULT '0',
  `extra_homepromo` tinyint(1) NOT NULL DEFAULT '0',
  `extra_homehr` tinyint(1) NOT NULL DEFAULT '0',
  `createdat` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatedat` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dump dei dati per la tabella `profiles_features`
--

INSERT INTO `profiles_features` (`id`, `profile_id`, `badge_sitelink`, `badge_companysheet`, `search_category`, `search_location`, `search_keywords`, `company_baseinfo`, `company_description`, `company_address`, `company_multipleaddress`, `company_sociallink`, `company_services`, `company_reviews`, `company_favorites`, `company_headerimg`, `company_promo`, `company_hr`, `company_photogallery`, `company_keywords`, `company_categories`, `extra_homecompanybanner`, `extra_homepromo`, `extra_homehr`, `createdat`, `updatedat`) VALUES
(1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2015-06-06 09:30:43', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struttura della tabella `profiles_types`
--

DROP TABLE IF EXISTS `profiles_types`;
CREATE TABLE IF NOT EXISTS `profiles_types` (
`id` int(11) NOT NULL,
  `codeprofile` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `createdat` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatedat` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `active` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dump dei dati per la tabella `profiles_types`
--

INSERT INTO `profiles_types` (`id`, `codeprofile`, `title`, `description`, `icon`, `createdat`, `updatedat`, `active`) VALUES
(1, 'free', 'profiles_types_title_free', 'profiles_types_description_free', '', '2015-06-06 09:25:28', '0000-00-00 00:00:00', 1),
(2, 'silver', 'profiles_types_title_silver\r\n', 'profiles_types_description_silver', '', '2015-09-05 09:48:01', '0000-00-00 00:00:00', 1),
(3, 'gold', 'profiles_types_title_gold', 'profiles_types_description_gold', '', '2015-09-05 09:48:01', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `promo`
--

DROP TABLE IF EXISTS `promo`;
CREATE TABLE IF NOT EXISTS `promo` (
`id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `img` varchar(255) NOT NULL,
  `createdat` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatedat` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `reviews`
--

DROP TABLE IF EXISTS `reviews`;
CREATE TABLE IF NOT EXISTS `reviews` (
`id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `mail` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `stars` int(11) NOT NULL,
  `createdat` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatedat` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dump dei dati per la tabella `reviews`
--

INSERT INTO `reviews` (`id`, `company_id`, `mail`, `title`, `description`, `stars`, `createdat`, `updatedat`, `active`) VALUES
(1, 1, 'b&b@test.it', 'titled', '<p>descr</p>\r\n', 2, '2015-06-20 09:01:12', '0000-00-00 00:00:00', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `authtoken`
--
ALTER TABLE `authtoken`
 ADD PRIMARY KEY (`authUserID`);

--
-- Indexes for table `authuser`
--
ALTER TABLE `authuser`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companies_addresses`
--
ALTER TABLE `companies_addresses`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companies_gallery`
--
ALTER TABLE `companies_gallery`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companies_typologies`
--
ALTER TABLE `companies_typologies`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companies_typologies_old`
--
ALTER TABLE `companies_typologies_old`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_typologies_link`
--
ALTER TABLE `company_typologies_link`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expires_status`
--
ALTER TABLE `expires_status`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `languages_i18n`
--
ALTER TABLE `languages_i18n`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profiles_features`
--
ALTER TABLE `profiles_features`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profiles_types`
--
ALTER TABLE `profiles_types`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `promo`
--
ALTER TABLE `promo`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `authuser`
--
ALTER TABLE `authuser`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `companies_addresses`
--
ALTER TABLE `companies_addresses`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `companies_gallery`
--
ALTER TABLE `companies_gallery`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `companies_typologies`
--
ALTER TABLE `companies_typologies`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `companies_typologies_old`
--
ALTER TABLE `companies_typologies_old`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `company_typologies_link`
--
ALTER TABLE `company_typologies_link`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `expires_status`
--
ALTER TABLE `expires_status`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `languages_i18n`
--
ALTER TABLE `languages_i18n`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `log`
--
ALTER TABLE `log`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `profiles_features`
--
ALTER TABLE `profiles_features`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `profiles_types`
--
ALTER TABLE `profiles_types`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `promo`
--
ALTER TABLE `promo`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;