<?php
class errorHandling{
	private $language;
	public $errors;
        public $translation;
        /*
	private $config = array(
						'first_name' 	=> array('required'),
						'last_name' 	=> array('required'),
						'company' 		=> array('required'),
						'mail' 			=> array('email', 'required'),
						'privacy' 		=> array('required'),
						'photo'			=> array('file'),
						'profession' 	=> array('required'),
					);
         * 
         */
        private $config;
        protected $supportedImage = array('image/png', 'image/jpeg', 'image/gif');
	protected $maxSizeImage = 3000;

	function __construct($translation, $config){
		$this->translation = $translation;
                $this->config = $config;
	}
	public function isValid($form_values){
            
                if(count($form_values) != count($this->config))
                {
                    $this->errors['critical'] = $this->translation['RegisterParameter'];                                                        
                    return false;
                }
		//Each form input
		foreach ($form_values as $key => $value) {
			//Each input's validator
			if(isset($this->config[$key])){
				foreach ($this->config[$key] as $validator) {

					switch($validator){
						case 'required':
                                                        if(!isset($form_values[$key]))
                                                            $this->errors[$key] = $this->translation['Mandatory'];
							if(!$this->isNotEmpty($value))
								$this->errors[$key] = $this->translation['Mandatory'];                                                        
							break;
						case 'numeric':
							if(!$this->isNumeric($value))
								$this->errors[$key] = ucfirst($key).$this->translation['NotNumber'];
							break;
                                                case 'sex':
                                                        if($value != 'M' && $value != 'F')
                                                            $this->errors[$key] = $this->translation['NotValidSex'];
                                                        break;
						case 'date':
							if(!$this->isDate($value))
								$this->errors[$key] = $this->translation['NotValidDate'];
							break;
						case 'email':
							if(!$this->isEmail($value))
								$this->errors[$key] = $this->translation['NotValidEmail'];
							break;
						case 'image':
							if(!$this->isValidImage($value))
								$this->errors[$key] = $this->translation['NotValidFileFormat'];
							break;
                                                case 'vatcode':
                                                        if(!$this->isValidVatcode($value))
								$this->errors[$key] = $this->translation['NotValidVatCodeFormat'];
                                                        break;
                                                case 'geo':
                                                        if(!isset($form_values[$key]))
                                                            $this->errors[$key] = $this->translation['GeoError'];
							if(!$this->isNotEmpty($value))
								$this->errors[$key] = $this->translation['GeoError'];                                                        
							break;
						default:
							break;
					}
				}
			}
		}

		if(!is_null($this->errors))
			return false;
		else
			return true;
	}

	private function isNotEmpty($string){
		return $string !== '';
	}

	private function isEmail($email){
		return filter_var($email, FILTER_VALIDATE_EMAIL);
	}

	private function isDate($date){
		list($yyyy,$mm,$dd)=explode("-",$date);
	    if (is_numeric($yyyy) && is_numeric($mm) && is_numeric($dd))
	    {
	        return checkdate($mm,$dd,$yyyy);
	    }
	    return false;
	}

	private function isNumeric($value){
		return is_numeric($value);
	}

	private function isValidImage($file){
		if(!empty($file['type'])){
			if(!in_array($file['type'], $this->supportedImage))
				return false;
			//($_FILES["file"]["type"] == "image/png")
			$fsize = $file['size'] / 1024;
			if ($fsize > $this->maxSizeImage)
				return false;
		}

		return true;
	}
        
        private function isValidFile($file){
		if(!empty($file['type'])){
			if(!in_array($file['type'], $this->supported))
				return false;

                        $fsize = $file['size'] / 1024;
			if ($fsize > $this->maxSize)
				return false;
		}

		return true;
	}
        
        private function isValidVatcode($value)
        {
            if(eregi("[a-zA-Z]{6}[0-9]{2}[a-zA-Z][0-9]{2}[a-zA-Z][0-9]{3}[a-zA-Z]$", $value))
                return true;
            else
                return false;
        }
}