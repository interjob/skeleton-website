<?php

class Route{

  private function controllerExists($controllerName){
    if(file_exists(BASE_PATH."/controllers/$controllerName.php")){
      //if(class_exists($controllerName))
        return true;
    }
    return false;
  }

  protected function controller($shortName, $app, $db = null, $params = null)
  {
    list($shortClass, $shortMethod) = explode('/', $shortName, 2);

    $controller   = ucfirst($shortClass).'Controller';
    $fullName     = sprintf('%s::%s', $controller, $shortMethod);

    if( $this->controllerExists($controller) ){

      require BASE_PATH."/controllers/$controller.php";

        $arguments  = func_get_args();
        array_shift($arguments); //Remove the class and method name
        $reflect    = new ReflectionClass($controller);

//        if(!empty($params))
//        {
//           $arguments = array_merge($arguments, $params);
//        }

        if(func_num_args() > 0) {
          $instance = $reflect->newInstanceArgs($arguments);
        }
        else {
          $instance = $reflect->newInstance();
        }

        $bool = method_exists($instance, 'beforeAction');
        if(method_exists($instance, 'beforeAction'))
          call_user_func_array(array($instance, 'beforeAction'), array($shortMethod));

        $rt = array($instance, $shortMethod);
        return $rt;
    }
  }

  public function routeExecute($app, $slug, $params, $db = null){
    // $request = Slim::getInstance()->request();
    // $wine = json_decode($request->getBody());
    $getParameters = null;
    if(isset($_GET["data"]))
    {
      $getParameters = $_GET["data"];
      $getParameters = json_decode($getParameters);
      $getParameters = (array)$getParameters->params;
    }

    $slimRequest = $app->request()->getBody();
    $slimRequest = json_decode($slimRequest);

    if(isset($slimRequest->params))
      $slimRequest = (array)$slimRequest->params;

    if(is_array($slimRequest))
    {
      $params = array_merge($params, $slimRequest);
    }
    if(is_array($getParameters))
    {
      $params = array_merge($params, $getParameters);
    }

    call_user_func( $this->controller($slug, $app, $db, $params) );
    // try{
    //   call_user_func( $this->controller($slug, $app, $db, $params) );
    // } catch (Exception $e) {
    //   $app->redirect('/not-found');
    // }
  }
}

$Route = new Route();

$app->map('/hello', function() use($app, $db, $Route){
    $slimRequest = $app->request()->getBody();
    $slimRequest = json_decode($slimRequest);
    $slimRequest = (array)$slimRequest->params;
    echo "Hello ".$slimRequest['nome']." ".$slimRequest['cognome'];
})->via('IWANT');



//VIEWS


// index route
$app->get('/', function() use($app, $db, $Route){
    $Route->routeExecute($app, "Public/home", array(), $db);
});

//404 route
$app->get('/not-found',function(){
  echo 'Not Found';
});

//static pages
$app->get('/privacy', function() use($app, $db, $Route){    
  $Route->routeExecute($app, "Public/privacy", array(), $db);
});


/*
// search page example for GET and POST
$app->get('/search', function() use($app, $db, $Route){    
  $Route->routeExecute($app, "Public/search", array(), $db);
});
$app->post('/search', function() use($app, $db, $Route){    
  $Route->routeExecute($app, "Public/searchResult", array(), $db);
});
*/

/* 
//example with more parameters GET
$app->get('/editCompanyData/:company_slug/:accessKey', function($companySlug, $accessKey) use($app, $db, $Route){
  $Route->routeExecute($app, "Reserved/editCompanyData", array( "companySlug"=>$companySlug, "accessKey"=>$accessKey), $db);
});
*/

//paypal pages
/*
$app->get('/paypal', function() use($app, $db, $Route){    
  $Route->routeExecute($app, "Reserved/paypalForm", array(), $db);
});
$app->post('/paypal', function() use($app, $db, $Route){    
  $Route->routeExecute($app, "Reserved/paypal", array(), $db);
});
$app->get('/paypalCancel', function() use($app, $db, $Route){    
  $Route->routeExecute($app, "Reserved/paypalCancel", array(), $db);
});
$app->get('/paypalConfirm', function() use($app, $db, $Route){    
  $Route->routeExecute($app, "Reserved/paypalConfirm", array(), $db);
});
*/

if(DEBUG)
{
    $app->get('/resetDB', function()use($db){
        $db->reset();
        echo "DB Reset Done!";
    });
    
}