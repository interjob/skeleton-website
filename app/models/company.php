<?php

class Company extends baseModel
{
	//* public static method to insert company
	/**
		this method insert new company with passed parameters
		<pre>
		$db : PDO resource to connect and modify data
		$params : parameters to update
				Ex.
				array(
					'account_id'            => 1,
                                        'profile_id'            => 3,
                                        'company_name'          => 'Ziummo',                                        
                                        'company_url'           => 'http://www.salcazzo.it',
                                        'company_baseinfo'      => 'Siamo di alto livello',
                                        'company_description'   => 'Abbiamo iniziato...bla...bla',
					'social_facebook_link'  => 'http://facebook.com/salcazzo',
                                        'social_linkedin_link'  => 'http://linkedin.com/salcazzo',
                                        'company_headerimg'     => 'path/to/image',
                                        'company_keywords'      => 'fannulloni, alto livello, salcazzo',
					'createdat'             => '19900219 14:22:45',
					'updatedat' 		=> '19900219 14:22:45',
                                        'active' 		=> 1,
				)
		$userID : it's identifier of user that do this action
		</pre>
		return number of affected rows
	*/
	static public function insertCompany($db, $params, $userID)
	{
		$rs = self::get($db, $params, 'companies', $userID);

		if(isset($rs[0]))
			return ALREADY_IN;
		else
			return parent::insert($db, $params, 'companies', 'id', $userID);
	}
        
        
	//* public static method to insert address
	/**
		this method insert new company with passed parameters
		<pre>
		$db : PDO resource to connect and modify data
		$params : parameters to update
				Ex.
				array(
					'account_id'            => 1,                                     
                                        'company_address'       => 'via pilotino, 22',
                                        'active' 		=> 1,
				)
		$userID : it's identifier of user that do this action
		</pre>
		return number of affected rows
	*/
	static public function insertAddress($db, $params, $userID)
	{
		$rs = self::get($db, $params, 'companies_addresses', $userID);

		if(isset($rs[0]))
			return ALREADY_IN;
		else
                return parent::insert($db, $params, 'companies_addresses', 'id', $userID);

	}
        
	static public function deleteCompanyAddress($db, $params = array(), $userID)
	{
		//return parent::delete($db, $params, 'companies', $userID);
                $conditions=array("active"=>0);               
		return parent::update($db, $conditions, 'companies_addresses', $params, $userID);
	}        
        
	static public function getAddresses($db, $params = array(), $userID)
	{
                $params['active'] = 1;
		return parent::get($db, $params, 'companies_addresses', $userID);
	}        
        
	static public function getAddressesFromSlug($db, $slug, $userID)
	{
            $queryParams[":slug"]=$slug;
            $queryParams[":active"]=1;
            $query =" SELECT * FROM companies LEFT JOIN companies_addresses ON companies.id = companies_addresses.company_id WHERE companies.company_slug = :slug AND companies.active = :active AND companies_addresses.active = :active ORDER BY companies.profile_id DESC " ;
            return MyPDO::myExec($db, $query, $queryParams, 0);   
	}        
        
        
        
	//* public static method to insert address
	/**
		this method insert new company with passed parameters
		<pre>
		$db : PDO resource to connect and modify data
		$params : parameters to update
				Ex.
				array(
					'account_id'            => 1,                                     
                                        'company_address'       => 'via pilotino, 22',
                                        'active' 		=> 1,
				)
		$userID : it's identifier of user that do this action
		</pre>
		return number of affected rows
	*/
	static public function insertLinkCategory($db, $params, $userID)
	{
		$rs = self::get($db, $params, 'company_typologies_link', $userID);

		if(isset($rs[0]))
			return ALREADY_IN;
		else
                return parent::insert($db, $params, 'company_typologies_link', 'id', $userID);

	}

        
        
        
        
	//* public static method to update company
	/**
		this method update existing company with passed parameters
		<pre>
		$db : PDO resource to connect and modify data
		$params : parameters to update
				Ex.
				array(
					'account_id'            => 1,
                                        'profile_id'            => 3,
                                        'company_name'          => 'Ziummo',                                        
                                        'company_url'           => 'http://www.salcazzo.it',
                                        'company_baseinfo'      => 'Siamo di alto livello',
                                        'company_description'   => 'Abbiamo iniziato...bla...bla',
					'social_facebook_link'  => 'http://facebook.com/salcazzo',
                                        'social_linkedin_link'  => 'http://linkedin.com/salcazzo',
                                        'company_headerimg'     => 'path/to/image',
                                        'company_keywords'      => 'fannulloni, alto livello, salcazzo',
					'createdat'             => '19900219 14:22:45',
					'updatedat' 		=> '19900219 14:22:45',
                                        'active' 		=> 1,
				)
		$userID : it's identifier of user that do this action
		</pre>
		return number of affected rows
	*/
	static public function updateCompany($db, $params, $conditions, $userID)
	{
		return parent::update($db, $params, 'companies', $conditions, $userID);
	}

	//*public static method to delete company
	/**
		this method delete one entity row
		$db : PDO resource to connect and retrieve data
		$id : mandatory for find what to delete
		$userID : it's identifier of user that do this action
		return true or false if the operation do its job or not
	*/
	static public function deleteCompany($db, $params = array(), $userID)
	{
		//return parent::delete($db, $params, 'companies', $userID);
                $conditions=array("active"=>0);               
		return parent::update($db, $conditions, 'companies', $params, $userID);
	}

	//*public static method to get company
	/**
		this method return entity list or if id parameter is not null get details of one company entity
		$db : PDO resource to connect and retrieve data
		$id : optional parameter, if it is set get single company entity
		$userID : it's identifier of user that do this action
		return list of company or single company
	*/
	static public function getCompany($db, $params = array(), $userID)
	{
		return parent::get($db, $params, 'companies', $userID);
	}
        
	static public function getCompanyOrdered($db, $params = array(), $userID)
	{
		return parent::getOrdered($db, $params, 'companies', $userID, ORDERCOMPANY);
	}

}
