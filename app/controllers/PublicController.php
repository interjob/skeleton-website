<?php
class PublicController extends BaseController
{    
    private $errors = array();
    
    function __construct($app, $db, $parameters = '')
    {
            parent::__construct($app, $db);
            if(!empty($parameters))
            {
                    $this->parameters = $parameters;
                    if(isset($this->parameters['accessKey']))
                            $this->key = $this->parameters['accessKey'];
            }
            
            if(isset($parameters['errors']))
                $this->errors = unserialize(base64_decode($parameters['errors']));            
    }
    
    public function home()
    {
        $staticContent = array(
			'head' 		=> array('title' => $this->translation['titleHome'], 'style' => 'style'),
			'menu'		=> array(),
			'header' 	=> array('placeToSearch'=> $this->translation["placeToSearch"], 'categoriesSearch'=> $this->translation["categoriesSearch"],'keywordsSearch'=> $this->translation["keywordsSearch"] ),
			'home'		=> array(),
			'footer' 	=> array(),
		);
        $this->render('home', array(), $staticContent);
    }
            
    public function privacy()
    {
        $staticContent = array(
			'head' 		=> array('title' => $this->translation['titleHome'], 'style' => 'style'),
			'menu'		=> array(),
			'header' 	=> array('placeToSearch'=> $this->translation["placeToSearch"], 'categoriesSearch'=> $this->translation["categoriesSearch"],'keywordsSearch'=> $this->translation["keywordsSearch"] ),
			'privacy'	=> array('privacy_text' => $this->translation["privacy_text"], 'style'=> 'style'),
			'footer' 	=> array(),
		);
        $this->render('privacy', array(), $staticContent);
    }    
   

}
