<?php
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;

use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;

class ReservedController extends BaseController
{

	protected $publicAction = array('login','paypalForm','paypal','paypalConfirm','paypalCancel','testCompany','regenerateAllCompanies'); // HERE GO PUBLIC METHODS WITH NO AUTH
        protected $authActionLevel = array(
            'social_user',
            'user',
            'reseller',
            'admin'
        );
	private $auth = null;			

	function __construct($app, $db, $parameters = '')
	{
		parent::__construct($app, $db);
		if(!empty($parameters))
		{
			$this->parameters = $parameters;
			if(isset($this->parameters['accessKey'])){
				$this->key = $this->parameters['accessKey'];                        
                                unset($this->parameters["accessKey"]);
                        }
                        
		}                
	}

	public function beforeAction($calledAction)
	{                
		if(!in_array($calledAction, $this->publicAction)){
			$auth = new Auth($this->db, DEBUG);
			$auth->setTheKey($this->key);                        
			if(!$auth->areYouAutheticated()){
                            if ($this->app->request->isGet()){
                                $this->app->redirect("/");
                            }else{
				$this->app->halt(401, json_encode(array('message' => 'You shall not pass!', 'accessKey' => $this->key)));     
                            }
			}
			$this->key = $auth->key;
			$this->auth = $auth;                        
		}
	}
        
        public function setNewPasswordForm()
        {            
            //Maybe could be usefull set a password minimum requirements
            $staticContent = array(
                    'head' 		=> array('title' => $this->translation['titleSetPassword'], 'style' => 'style'),
                    'menu'		=> array(),
                    'header'            => array(),
                    'setPassword'       => array(),
                    'footer'            => array(),
            );
            $this->render('setPassword', array(), $staticContent);
        }
        
        public function showCompanyPage()
        {
            $companySlug=$this->parameters["company_slug"];
            $accountID = Account::getAccount($this->db, array("authuser_id"=>$this->auth->userID), $this->auth->userID); 
   
            $companies = Company::getCompany($this->db, array('company_slug'=>$companySlug), $this->auth->userID);
            
            if (!$companies ){
                    $this->jsonResponse(array("success" => false, "message"=>$this->translation['CompanyNotAvailable']),410, $this->key);

            }
            
            $paramsOutput=array("company_url"=>$companies[0]["company_url"]);
            
            
            //check if is your company
                if (!empty($accountID)){
                    if ($accountID[0]["id"]==$companies[0]["account_id"]){
                        //edit possible
                        $paramsOutput["editable"]=true;
                    }
                }
            //check gold or silver profiles
                //get additional data -> save in params
            
            $this->jsonResponse($paramsOutput, 200, $this->key);
            
        }
        
/*        
        public function testCompany() 
        {                                    
            $companySlug="test"; // o test company da modificare
            $staticContent = array(
                    'head' 		=> array('title' => $this->translation['titleHome'], 'style' => 'style'),
                    'menu'		=> array(),
                    'header'            => array(),
                    'addCompany'	=> array(),
                    'footer'            => array("script"=>"company.js"),
            );
            $this->getView('addCompany', $staticContent);
        }
*/

        public function setNewPassword()
        {
            $userID = $this->auth->userID; 
            $rs = Auth::updateAuthUser($this->db, array( 'pass' => hash(CRYPT_METHOD,$this->parameters["password"]) ), array('id' => $userID), $userID);
            if($rs == 0)
            {

               // $response->body(json_encode(array('success' => false,  'message' => $this->translation['passwordSetError'])));                
                $this->jsonResponse(array("success" => false, "message"=>$this->translation['passwordSetError']),400, $this->key);                
                
                
                return;
            }
            else
            {

                //$response->body(json_encode(array('success' => true,  'errors' => null, 'message' => $this->translation['passwordCorrectlySet'])));
                $this->jsonResponse(array("success" => true, "message"=>$this->translation['passwordCorrectlySet']),200, $this->key);                  
                return;
            }
        }
        
	public function login()
	{                                
                $auth = new Auth($this->db, DEBUG);
                
                if(isset($this->parameters['username']))
                {
                    $auth->username = $this->parameters['username'];
                }
                if(isset($this->parameters['password']))    
                {
                    $auth->password = hash(CRYPT_METHOD,$this->parameters["password"]);
                }
                if(!$auth->areYouAutheticated())
                {
                    $rs = Auth::getAuthUser($this->db, array( 'email' => $this->parameters['username'], 'pass' => $auth->password ), 0);
                    if(!empty($rs))
                    {
                        $this->jsonResponse(array('success' => false, 'message' => $this->translation['accountNotVerified']), 401, $this->key);                        
                        return;
                    }
                    else
                    {
                        $this->jsonResponse(array('success' => false, 'message' => $this->translation['loginFailed']), 401, $this->key);
                        return;
                    }                        
                }else{
                    $this->key = $auth->key;
                    $this->auth = $auth;
                    $this->jsonResponse(array('success' => true, 'message' => $this->translation['loginSuccess']), 200, $this->key);
                    return; 
                }                                
	}
        
        public function logout()
        {
            //$newKey = $this->auth->resetKey($this->auth->username);
            $newKey = $this->auth->resetKey($this->auth->userID);
            if(!is_null($newKey))
               /* $this->jsonResponse(array('message' => 'Logout successfully!'), 200, null);*/            
                $this->jsonResponse(array('success' => true, 'message' => $this->translation['LogoutSuccess']), 200, null);            
            else
                $this->jsonResponse(array('success' => false, 'message' => $this->translation['LogoutFailed']), 501, null);                    
                /*$this->jsonResponse(array('message' => 'Error!'), 406, $this->key);*/
        }
        
        public function accountDashboard()
        {
            $accountID = Account::getAccount($this->db, array("authuser_id"=>$this->auth->userID), $this->auth->userID);
            $companies = Company::getCompany($this->db, array('account_id'=>$accountID[0]['id'],"active"=>1), $this->auth->userID);
            $template_company_list = "";

            foreach ($companies as $company) {
                
                //if company is free
                    if ($company["profile_id"]==1){    
                        $classRemoveCompany="hide";
                    }else{
                        $classRemoveCompany="";
                    }                
                
                $template_company_list.= $this->getView('companyBadge', 
                                                                        array('company_name' => $company['company_name'], 
                                                                              'upgrade_profile' => $this->translation['upgrade_profile'], 
                                                                              'edit_profile' => $this->translation['edit_profile'],
                                                                              'addresses' => $this->translation['edit_addresses'],
                                                                              'promos' => $this->translation['edit_promos'],
                                                                              'jobs' => $this->translation['edit_jobs'],
                                                                              'swaps' => $this->translation['edit_swaps'],
                                                                              'company_slug'=>$company["company_slug"],
                                                                              'hideManageCompany'=>$classRemoveCompany, 
                                                                              'CompanyLink' => $company["company_slug"].".html")
                                                       );
            }

            $staticContent = array(
                            'head' 		=> array('title' => $this->translation['titleHome'], 'style' => 'style'),
                            'menu'		=> array(),
                            'headerReserved'            => array(),
                            'accountDashboard'	=> array('template_company_list'=>$template_company_list),
                            'footer'            => array("script"=>"company.js"),
                    );
            $this->render('accountDashboard', array(), $staticContent);
        }             
        
        public function addCompany()
        {
            
            $staticContent = array(
                            'head' 		=> array('title' => $this->translation['titleHome'], 'style' => 'style'),
                            'menu'		=> array(),
                            'headerReserved'    => array(),
                            'addCompany'	=> array(),
                            'footer'            => array("script"=>"company.js"),
                    );
            $this->render('addCompany', array(), $staticContent);
        }    
        
        
        public function removeCompany()
        {
            //POST SUBMIT
            
            $data = $this->app->request()->params();
            $data = array_merge($data, $this->parameters);      
            //print_r($data);
            $companyId = Company::deleteCompany($this->db, $data, $this->auth->userID);    
            
            if ($companyId !==0){
                $this->jsonResponse(array('success' => true, 'message' => $this->translation['OperationSuccess']), 200, $this->key);
            }else{
                $this->jsonResponse(array('success' => false, 'message' => $this->translation['OperationFailed']), 200, $this->key);
            }
        }             

        public function newCompany()
        {
            $data = $this->app->request()->params();
            $data = array_merge($data, $this->parameters);
            $data["profile"]=1; //FFFFFFFFFFFUUUUCK someone <--- validatore
            $validator = array(

                'profile'               =>  array('required'),
                'company_name'          =>  array('required'),
                'company_address'       =>  array('required'),
                'company_city'          =>  array('required'),
                'company_state'         =>  array('required'),
                'company_cap'           =>  array('required'),
                'company_country'       =>  array('required'),
                'latgeo'                =>  array('geo'),
                'longgeo'               =>  array('geo'),
                'company_url'           =>  array(''),
                'categories'            =>  array(''),
                'company_baseinfo'      =>  array(''),
                'company_description'   =>  array(''),
                'social_facebook_link'  =>  array(''),
                'social_linkedin_link'  =>  array(''),            
                'company_keywords'      =>  array(''),
                'privacy'               =>  array('required')
            );
                                                                    
            if(isset($_FILES['company_headerimg']))
            {
                $data['company_headerimg'] = $_FILES['company_headerimg'];
                $validator['company_headerimg'] = array('image');
            }
            if(isset($_FILES['company_logo']))
            {
                $data['company_logo'] = $_FILES['company_logo'];
                $validator['company_logo'] = array('required', 'image');
            }

            $validation = new errorHandling($this->translation, $validator);
            $validationRs = $validation->isValid($data);

            if(isset($validation->errors['critical'])){	
                
                   /* $response = $this->app->response();
                    $response['Content-Type'] = 'application/json';
                    $response['X-Powered-By'] = POWERED_BY;
                    $response->status(400);
                    $response->body(json_encode(array('success' => false,  'errors' => $validation->errors)));*/
                    
                $this->jsonResponse(array("success" => false, "errors" => $validation->errors, "message"=>$this->translation['OperationFailed']) ,406, $this->key);      
                
                    return;
            }

            if(!empty($validation->errors)){	
                /*
                    $response = $this->app->response();
                    $response['Content-Type'] = 'application/json';
                    $response['X-Powered-By'] = POWERED_BY;
                    $response->status(200);
*/
                    //$response->body(json_encode(array('success' => false,  'errors' => $validation->errors)));
                
                $this->jsonResponse(array("success" => false, "errors" => $validation->errors, "message"=>$this->translation['OperationFailed']) ,200, $this->key);                       
                    return;
            }
            ///TODO UPLOAD IMAGE AND METHOD TO INSERT ACCOUNT AND USER
            if(isset($_FILES['company_headerimg']['name']))
            {
                $newImageName = date("Ymd").uniqid().urlencode($_FILES['company_headerimg']['name']);
                if($this->uploadImage($_FILES['company_headerimg'], $newImageName))
                        $data['company_headerimg'] = $newImageName;
            }
            if(isset($_FILES['company_logo']['name']))
            {
                $newImageName = date("Ymd").uniqid().urlencode($_FILES['company_logo']['name']);
                if($this->uploadImage($_FILES['company_logo'], $newImageName))
                        $data['company_logo'] = $newImageName;
            }
            $rs = $this->insertCompany($data); 
                        
                        
            if($rs === ALREADY_IN){
                /*
                $response = $this->app->response();
                $response['Content-Type'] = 'application/json';
                $response['X-Powered-By'] = POWERED_BY;
                $response->status(406);
                $response->body(json_encode(array('success' => false,  'errors' => array('critical' => $validation->translation['userAlreadyExists']))));
                */
                
                $this->jsonResponse(array("success" => false, "errors" => $validation->errors, "message"=>$this->translation['userAlreadyExists']) ,409, $this->key);                  
            }
            
            elseif($rs === false){
               /* 
                $response->body(json_encode(array('success' => false,  'errors' => array('critical' => $validation->translation['insertFailed']))));*/
                
                $this->jsonResponse(array("success" => false, "message"=>$this->translation['insertFailed']) ,400, $this->key);                 
                
            }
            else
            {

               // $response->body(json_encode(array('success' => true,  'errors' => null, 'message' => str_replace(':name', $data['name'], $validation->translation['successMessage']))));                
                $this->jsonResponse(array("success" => true, "message"=>str_replace(':name', $data['name'], $validation->translation['successMessage'])) ,200, $this->key);                   
                return;                
            }
        }

   private function insertCompany($data)
    {

       $accountID = Account::getAccount($this->db, array("authuser_id"=>$this->auth->userID));

            $account = array(
                'account_id'            => $accountID[0]["id"],
                'profile_id'            => $data['profile'],
                'company_slug'          => $this->NewCompanySlug($data["company_name"],$data["keywords"]),
                'company_name'          => $data['company_name'],
                'company_url'           => $data['company_url'],
                'company_baseinfo'      => $data['company_baseinfo'],
                'company_description'   => $data['company_description'],
                'social_facebook_link'  => $data['social_facebook_link'],
                'social_linkedin_link'  => $data['social_linkedin_link'],
                'company_keywords'      => $data['company_keywords']
            );
                        
/*
            if(isset($data['profile_img']))
                $account['profile_img'] = $data['profile_img'];
*/
        $companyId = Company::insertCompany($this->db, $account, $this->auth->userID);

        
        //insert addresses
        if ($companyId){
            
            $address = array(                
                "company_id" => $companyId,
                "address" => $data["company_address"],
                "city" => $data["company_city"],
                "state" => $data["company_state"],
                "cap" => $data["company_cap"],
                "country" => $data["company_country"],
                "latgeo" => $data["latgeo"],
                "longgeo" => $data["longgeo"]                             
                );
            
            $rs = Company::insertAddress($this->db, $address, $this->auth->userID);
                //var_dump($address);
                //var_dump($rs);
                if (!$rs){
                    die("error insert address");
                }   
                
                
        //insert categories
                //array di categorie 1=> id_categories
      //  var_dump ($data["categories"] );
        foreach ($data["categories"] as $value) {            
            $category = array(                
                "id_company" => $companyId,
                "id_typology" => $value,                        
                );
            
            $rs = Company::insertLinkCategory($this->db, $category, $this->auth->userID);        
                if (!$rs){
                    die("error category address");
                }           
            }
                               
        }
        

        
        
        //da spostare TO BE MOVED
                  $this->createHtmlCompany($account['company_slug']); //DB SE NON LO SPOSTI T'NCULO
        //SOMEONE CULO
        
        
        if($companyId != false && $companyId != ALREADY_IN)
        {    
            return true;
        }
                   
        return $companyId;
    }       
     
    
    private function NewCompanySlug($companyName,$keywords){
         /*       
        $categoryTitle= baseModel::get($this->$db, array("id"=>$categoryId), 'companies_typologies', $this->auth->userID);
        $categoryTitle= baseModel::get($this->$db, array("code_key"=>$categoryTitle[0]["title"]), 'languages_i18n', $this->auth->userID);*/
        $slug=  uniqid()."-".$companyName."-".str_replace(";", ".", $keywords);   
        
        return $slug;
    }
    
    
    
    // edit data company
    public function editCompanyData(){
                
        $company = Company::getCompany($this->db, array('company_slug'=>$this->parameters["companySlug"]), $this->auth->userID);                            
        $profileType= profileType::getProfileType($this->db, array("id" =>$company[0]["profile_id"]), $this->auth->userID);
        
        
        $dataCompany = array(
                'account_id'            => $accountID[0]["id"],
                'profile_id'            => $company[0]['profile'],
                'company_slug'            => $company[0]['company_slug'],
                'company_name_value'          => $company[0]['company_name'],
                'company_subtitle_value'          => $company[0]['company_subtitle'],
                'company_url_value'           => $company[0]['company_url'],
                'company_baseinfo_value'      => $company[0]['company_baseinfo'],
                'company_description_value'   => $company[0]['company_description'],
                'company_services_value'   => $company[0]['company_services'],
                'social_facebook_link_value'  => $company[0]['social_facebook_link'],
                'social_linkedin_link_value'  => $company[0]['social_linkedin_link'],
                'company_keywords_value'      => $company[0]['company_keywords']
            );
        
            //Maybe could be usefull set a password minimum requirements
            $staticContent = array(
                    'head' 		=> array('title' => $this->translation['titleSetPassword'], 'style' => 'style'),
                    'menu'		=> array(),
                    'header'            => array(),
                    'editCompanyData'   => $dataCompany,
                    'footer'            => array("script"=>"company.js"),
            );
            $this->render('setPassword', array(), $staticContent);        
                
    }
    
    public function updateCompanyData(){
            $data = $this->app->request()->params();
            $data = array_merge($data, $this->parameters);
            

            
            //validation
            
            $validator = array(

            
                  'company_name'          =>  array('required'),
                  'company_subtitle'      =>  array(''),
                  'company_url'           =>  array(''),
                  'categories'            =>  array(''),
                  'company_baseinfo'      =>  array(''),
                  'company_description'   =>  array(''),
                  'company_services'      =>  array(''),
                  'social_facebook_link'  =>  array(''),
                  'social_linkedin_link'  =>  array(''),            
                  'company_keywords'      =>  array(''),
              );

              $validation = new errorHandling($this->translation, $validator);
              $validationRs = $validation->isValid($data);
            
            if(isset($validation->errors['critical'])){	                                   
                $this->jsonResponse(array("success" => false, "errors" => $validation->errors, "message"=>$this->translation['OperationFailed']) ,406, $this->key);                      
                return;
            }

            if(!empty($validation->errors)){	                
                $this->jsonResponse(array("success" => false, "errors" => $validation->errors, "message"=>$this->translation['OperationFailed']) ,200, $this->key);                       
                return;
            }            
            
            
            //update
                                                
            $account = array(
                'company_name'          => $data['company_name'],
                'company_subtitle'      => $data['company_subtitle'],
                'company_url'           => $data['company_url'],
                'company_baseinfo'      => $data['company_baseinfo'],
                'company_services'      => $data['company_services'],
                'company_description'   => $data['company_description'],
                'social_facebook_link'  => $data['social_facebook_link'],
                'social_linkedin_link'  => $data['social_linkedin_link'],
                'company_keywords'      => $data['company_keywords']
            );            
            
            
            $rs = Company::updateCompany($this->db, $account, array("company_slug"=>$data["company_slug"]), $this->auth->userID);
            
            if($rs === false){
               /* 
                $response->body(json_encode(array('success' => false,  'errors' => array('critical' => $validation->translation['insertFailed']))));*/
                
                $this->jsonResponse(array("success" => false, "message"=>$this->translation['insertFailed']) ,400, $this->key);                 
                
            }
            else
            {

               // $response->body(json_encode(array('success' => true,  'errors' => null, 'message' => str_replace(':name', $data['name'], $validation->translation['successMessage']))));                
                $this->jsonResponse(array("success" => true, "message"=>str_replace(':name', $data['company_name'], $this->translation['successMessageUpdateCompany'])) ,200, $this->key);                   
                return;                
            }            
            
    }
    
   
        // edit data company
    public function editAddresses(){
                
        $companyAddresses = Company::getAddressesFromSlug($this->db, $this->parameters["companySlug"], $this->auth->userID);      
        //$profileType= profileType::getProfileType($this->db, array("id" =>$company[0]["profile_id"]), $this->auth->userID);
               
        $template_addresses_list="";
        foreach ($companyAddresses as $address) {
                    $template_addresses_list.= $this->getView('addressesBadge', 
                                                    array(
                                                          'addressLabel' => $this->translation['addressLabel'],
                                                          'cityLabel' => $this->translation['cityLabel'],
                                                          'capLabel' => $this->translation['capLabel'],
                                                          'countryLabel' => $this->translation['countryLabel'],                                                        
                                                          'address_id'=>$address["id"],                                                        
                                                          'address' => $address['address'], 
                                                          'city' => $address['city'],
                                                          'state' => $address['state'],
                                                          'cap' => $address['cap'],
                                                          'country' => $address['country'],
                                                          'company_slug'=>$address["company_slug"],
                                                          'traslatedMessageRemove' => $this->translation['traslatedMessageRemove'],
                                                          'removeAddress' => $this->translation['removeAddress']      
                                                        )
                                       );
        }
        
        //$profileType= profileType::getProfileType($this->db, array("id" =>$company[0]["profile_id"]), $this->auth->userID);
        
            $staticContent = array(
                    'head' 		=> array('title' => $this->translation['titleSetPassword'], 'style' => 'style'),
                    'menu'		=> array(),
                    'headerReserved'    => array(),
                    'editAddresses'     => array("template_addresses_list"=>$template_addresses_list,
                                                'NewAddress'=>$this->translation['NewAddress'],
                                                'company_slug'=>$this->parameters["companySlug"]
                                              ),
                    'footer'          => array("script"=>"company.js"),
            );
            $this->render('setPassword', array(), $staticContent);        
                
    }
    
    public function addAdressCompany(){

        
        $company = Company::getCompany($this->db, array("company_slug"=>$this->parameters["company_slug"]), $this->auth->userID);
        $companyId = $company[0]["id"];
        $rs = 0;
               
        //insert addresses
        if ($companyId){

            $address = array(                
                "company_id" => $companyId,
                "address" => $this->parameters["company_address"],
                "city" => $this->parameters["company_city"],
                "state" => $this->parameters["company_state"],
                "cap" => $this->parameters["company_cap"],
                "country" => $this->parameters["company_country"],
                "latgeo" => $this->parameters["latgeo"],
                "longgeo" => $this->parameters["longgeo"]                             
                );

            $rs = Company::insertAddress($this->db, $address, $this->auth->userID);

             

            if (!$rs){
                die("error insert address");
            }                   
        }

        if ($rs !== 0){
            $this->jsonResponse(array('success' => true, 'message' => $this->translation['OperationSuccess']), 200, $this->key);
        }else{
            $this->jsonResponse(array('success' => false, 'message' => $this->translation['OperationFailed']), 200, $this->key);
        }
    }
    
    public function removeAdressCompany(){
                  
     $res= Company::deleteCompanyAddress($this->db, $params = array("id"=>$this->parameters["idAddressRemove"]), $this->auth->userID); 
     
        if ($res !==0){
                    $this->jsonResponse(array('success' => true, 'message' => $this->translation['OperationSuccess']), 200, $this->key);
        }else{
            $this->jsonResponse(array('success' => false, 'message' => $this->translation['OperationFailed']), 200, $this->key);
        }
    }
    
    
    
    private function createHtmlCompany($data_slug)
    {
        

       //input data 
        // $accountID 
        // datacompany (only base)
        
        //get filecontent companyTemplate.php
        //populate data, merge header footer
        //save new file, slug used as html name 
                      
        $company = Company::getCompany($this->db, array('company_slug'=>$data_slug), $this->auth->userID);
                            
        $profileType= profileType::getProfileType($this->db, array("id" =>$company[0]["profile_id"]), $this->auth->userID);
 
        
        //regenerated only when not free 
        if ($profileType[0]["codeprofile"]!="free"){
        
                if (empty($company)) die("error data acquisition");

                $head=$this->getView('head', array("title" => $company[0]["company_name"]));
                $header=$this->getView('header', array());
                $body=$this->getView('company'.ucfirst($profileType[0]["codeprofile"]).'Template', array(
                    "company_name" => $company[0]["company_name"],
                    "company_url" => $company[0]["company_url"]                 //da completare x db
                ));
                $footer=$this->getView('footer', array());

               $handle=  fopen("companies/".$data_slug.".html", "w+");
               if ($handle){
                   fwrite($handle, $head.$header.$body.$footer);           
               }else{
                   die("error creating company file");
               }
               fclose($handle);
        }else{
            unlink("companies/".$data_slug.".html");
        }
    }    
    
        public function regenerateAllCompanies(){            
                    $companies = Company::getCompany($this->db, array(), $this->auth->userID);
                    foreach ($companies as $company) {
                        $this->createHtmlCompany($company["company_slug"]);
                    }
        }

    
        //PAYPAL 
        public function paypalForm(){
            
        $staticContent = array(
			'head' 		=> array('title' => $this->translation['titleHome'], 'style' => 'style'),
			'menu'		=> array(),
			'header' 	=> array(),
			'paypalForm'	=> array(),
			'footer' 	=> array(),
		);
        $this->render('paypalForm', array(), $staticContent);
                
        }        
        //PAYPAL 
        public function paypal()
        {
            $PaymentOption = "PayPal";
            if ( $PaymentOption == "PayPal")
            {
                    // ==================================
                    // PayPal Express Checkout Module
                    // ==================================



                    //'------------------------------------
                    //' The paymentAmount is the total value of 
                    //' the purchase.
                    //'
                    //' TODO: Enter the total Payment Amount within the quotes.
                    //' example : $paymentAmount = "15.00";
                    //'------------------------------------

                    $paymentAmount = "15.00";


                    //'------------------------------------
                    //' The currencyCodeType  
                    //' is set to the selections made on the Integration Assistant 
                    //'------------------------------------
                    $currencyCodeType = "EUR";
                    $paymentType = "Sale";

                    //'------------------------------------
                    //' The returnURL is the location where buyers return to when a
                    //' payment has been succesfully authorized.
                    //'
                    //' This is set to the value entered on the Integration Assistant 
                    //'------------------------------------
                    $returnURL = "http://www.linkforall.it/paypalConfirm";

                    //'------------------------------------
                    //' The cancelURL is the location buyers are sent to when they hit the
                    //' cancel button during authorization of payment during the PayPal flow
                    //'
                    //' This is set to the value entered on the Integration Assistant 
                    //'------------------------------------
                    $cancelURL = "http://www.linkforall.it/paypalCancel";

                    //'------------------------------------
                    //' Calls the SetExpressCheckout API call
                    //'
                    //' The CallSetExpressCheckout function is defined in the file PayPalFunctions.php,
                    //' it is included at the top of this file.
                    //'-------------------------------------------------


                            $items = array();
                            $items[] = array('name' => 'Item Name', 'amt' => $paymentAmount, 'qty' => 1);

                            //::ITEMS::

                            // to add anothe item, uncomment the lines below and comment the line above 
                            // $items[] = array('name' => 'Item Name1', 'amt' => $itemAmount1, 'qty' => 1);
                            // $items[] = array('name' => 'Item Name2', 'amt' => $itemAmount2, 'qty' => 1);
                            // $paymentAmount = $itemAmount1 + $itemAmount2;

                            // assign corresponding item amounts to "$itemAmount1" and "$itemAmount2"
                            // NOTE : sum of all the item amounts should be equal to payment  amount 
                            $paypal = new paypal();
                            $resArray = $paypal->SetExpressCheckoutDG( $paymentAmount, $currencyCodeType, $paymentType, 
                                                                                                            $returnURL, $cancelURL, $items );

                    $ack = strtoupper($resArray["ACK"]);
                    if($ack == "SUCCESS" || $ack == "SUCCESSWITHWARNING")
                    {
                            $token = urldecode($resArray["TOKEN"]);
                            $paypal->RedirectToPayPalDG( $token );
                    } 
                    else  
                    {
                            //Display a user friendly Error on the page using any of the following error information returned by PayPal
                            $ErrorCode = urldecode($resArray["L_ERRORCODE0"]);
                            $ErrorShortMsg = urldecode($resArray["L_SHORTMESSAGE0"]);
                            $ErrorLongMsg = urldecode($resArray["L_LONGMESSAGE0"]);
                            $ErrorSeverityCode = urldecode($resArray["L_SEVERITYCODE0"]);

                            echo "SetExpressCheckout API call failed. ";
                            echo "Detailed Error Message: " . $ErrorLongMsg;
                            echo "Short Error Message: " . $ErrorShortMsg;
                            echo "Error Code: " . $ErrorCode;
                            echo "Error Severity Code: " . $ErrorSeverityCode;
                    }
            }    
        }        
        public function paypalCancel(){
            
        $staticContent = array(
			'head' 		=> array('title' => $this->translation['titleHome'], 'style' => 'style'),
			'menu'		=> array(),
			'header' 	=> array(),
			'paypalCancel'	=> array(),
			'footer' 	=> array(),
		);
        $this->render('paypalCancel', array(), $staticContent);
                
        }   
        
        public function paypalConfirm(){
            
            $PaymentOption = "PayPal";
            if ( $PaymentOption == "PayPal" )
            {
                $paypal = new paypal();
                $res = $paypal->GetExpressCheckoutDetails( $_REQUEST['token'] );
                $finalPaymentAmount =  $res["PAYMENTREQUEST_0_AMT"];
                $token 				= $_REQUEST['token'];
                $payerID 			= $_REQUEST['PayerID'];
                $paymentType 		= 'Sale';
                $currencyCodeType 	= $res['CURRENCYCODE'];
                $items = array();
                $i = 0;
                // adding item details those set in setExpressCheckout
                while(isset($res["L_PAYMENTREQUEST_0_NAME$i"]))
                {
                        $items[] = array('name' => $res["L_PAYMENTREQUEST_0_NAME$i"], 'amt' => $res["L_PAYMENTREQUEST_0_AMT$i"], 'qty' => $res["L_PAYMENTREQUEST_0_QTY$i"]);
                        $i++;
                }

                $resArray = ConfirmPayment ( $token, $paymentType, $currencyCodeType, $payerID, $finalPaymentAmount, $items );
                $ack = strtoupper($resArray["ACK"]);
                if( $ack == "SUCCESS" || $ack == "SUCCESSWITHWARNING" )
                {
                    $transactionId		= $resArray["PAYMENTINFO_0_TRANSACTIONID"]; // Unique transaction ID of the payment.
                    $transactionType 	= $resArray["PAYMENTINFO_0_TRANSACTIONTYPE"]; // The type of transaction Possible values: l  cart l  express-checkout
                    $paymentType		= $resArray["PAYMENTINFO_0_PAYMENTTYPE"];  // Indicates whether the payment is instant or delayed. Possible values: l  none l  echeck l  instant
                    $orderTime 			= $resArray["PAYMENTINFO_0_ORDERTIME"];  // Time/date stamp of payment
                    $amt				= $resArray["PAYMENTINFO_0_AMT"];  // The final amount charged, including any  taxes from your Merchant Profile.
                    $currencyCode		= $resArray["PAYMENTINFO_0_CURRENCYCODE"];  // A three-character currency code for one of the currencies listed in PayPay-Supported Transactional Currencies. Default: USD.
                    $feeAmt				= $resArray["PAYMENTINFO_0_FEEAMT"];  // PayPal fee amount charged for the transaction
            //	$settleAmt			= $resArray["PAYMENTINFO_0_SETTLEAMT"];  // Amount deposited in your PayPal account after a currency conversion.
                    $taxAmt				= $resArray["PAYMENTINFO_0_TAXAMT"];  // Tax charged on the transaction.
            //	$exchangeRate		= $resArray["PAYMENTINFO_0_EXCHANGERATE"];  // Exchange rate if a currency conversion occurred. Relevant only if your are billing in their non-primary currency. If the customer chooses to pay with a currency other than the non-primary currency, the conversion occurs in the customer's account.
                    $paymentStatus = $resArray["PAYMENTINFO_0_PAYMENTSTATUS"];
                    $pendingReason = $resArray["PAYMENTINFO_0_PENDINGREASON"];
                    $reasonCode	= $resArray["PAYMENTINFO_0_REASONCODE"];
                    
                    $staticContent = array(
                                    'head' 		=> array('title' => $this->translation['titleHome'], 'style' => 'style'),
                                    'menu'		=> array(),
                                    'header'            => array(),
                                    'paypalConfirm'	=> array(),
                                    'footer'            => array(),
                            );
                    $this->render('paypalConfirm', array(), $staticContent);
                }
                else
                {
                    //Display a user friendly Error on the page using any of the following error information returned by PayPal
                    $ErrorCode = urldecode($resArray["L_ERRORCODE0"]);
                    $ErrorShortMsg = urldecode($resArray["L_SHORTMESSAGE0"]);
                    $ErrorLongMsg = urldecode($resArray["L_LONGMESSAGE0"]);
                    $ErrorSeverityCode = urldecode($resArray["L_SEVERITYCODE0"]);
                    //TO BE PRINTED
                }
            }        
                
        }    
        
        
        
        
        private function getApiContext($clientId, $clientSecret)
            {
                // #### SDK configuration
                // Register the sdk_config.ini file in current directory
                // as the configuration source.
                /*
                if(!defined("PP_CONFIG_PATH")) {
                    define("PP_CONFIG_PATH", __DIR__);
                }
                */
                // ### Api context
                // Use an ApiContext object to authenticate
                // API calls. The clientId and clientSecret for the
                // OAuthTokenCredential class can be retrieved from
                // developer.paypal.com
                $apiContext = new ApiContext(
                    new OAuthTokenCredential(
                        $clientId,
                        $clientSecret
                    )
                );
                // Comment this line out and uncomment the PP_CONFIG_PATH
                // 'define' block if you want to use static file
                // based configuration
                $apiContext->setConfig(
                    array(
                        'mode' => PAYPAL_MODE,
                        'log.LogEnabled' => true,
                        'log.FileName' => '../PayPal.log',
                        'log.LogLevel' => 'DEBUG', // PLEASE USE `FINE` LEVEL FOR LOGGING IN LIVE ENVIRONMENTS
                        'validation.level' => 'log',
                        'cache.enabled' => true,
                        // 'http.CURLOPT_CONNECTTIMEOUT' => 30
                        // 'http.headers.PayPal-Partner-Attribution-Id' => '123123123'
                    )
                );
                // Partner Attribution Id
                // Use this header if you are a PayPal partner. Specify a unique BN Code to receive revenue attribution.
                // To learn more or to request a BN Code, contact your Partner Manager or visit the PayPal Partner Portal
                // $apiContext->addRequestHeader('PayPal-Partner-Attribution-Id', '123123123');
                return $apiContext;
            }

}
